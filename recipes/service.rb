#
# Cookbook:: greenbone-cookbook
# Recipe:: service
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

include_recipe 'runit::default'
%w(scanner manager assistant).each do |s|
  runit_service s do
    default_logger true
    service_name node['greenbone'][s]['service_name']
    options({
      :user => 'root',
      :binary => node['greenbone'][s]['service_binary'],
      :parameters => node['greenbone'][s]['service_parameters']
    }.merge(params))
    action s == 'assistant' ? [:create, :enable, :stop] : [:create, :enable, :start]
  end
end
