#
# Cookbook:: greenbone
# Recipe:: default
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

include_recipe 'greenbone::source'

include_recipe 'greenbone::redis'
# TODO: check on PostgreSQL enabled
include_recipe 'greenbone::sqlite3'
# to increase kernel entropy for GPG key generation during 'openvasmd --rebuild'
package 'haveged'

node['greenbone']['additional_packages'].each do |p|
  package p
end

include_recipe 'greenbone::service'
include_recipe 'greenbone::configure'
