#
# Cookbook:: greenbone-cookbook
# Recipe:: configure
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

execute 'openvas-manage-certs -a' do
  action :run
  not_if "#{File.join(node['greenbone']['install']['dir'], 'bin', 'openvas-manage-certs')} -V"
  notifies :start, 'runit_service[assistant]', :immediately
end

execute 'openvasmd --rebuild' do
  command %Q[#{File.join(node['greenbone']['install']['dir'], 'sbin', 'openvasmd')} --rebuild]
  action :run
end

execute 'openvasmd --migrate' do
  command %Q[#{File.join(node['greenbone']['install']['dir'], 'sbin', 'openvasmd')} --migrate]
  action :run
end

template File.join(node['greenbone']['install']['dir'], 'bin', 'greenbone-sync') do
  source 'greenbone-sync.erb'
  owner 'root'
  group 'root'
  mode   0744
  action :create
end
