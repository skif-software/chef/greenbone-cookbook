#
# Cookbook:: greenbone-cookbook
# Recipe:: redis
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

package 'redis-server'

template '/etc/redis/redis.conf' do
  source 'redis.conf.erb'
  action :create
end

service 'redis' do
  action :restart
end

link '/tmp/redis.sock' do
  to '/var/run/redis/redis.sock'
end
