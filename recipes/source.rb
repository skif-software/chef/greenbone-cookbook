#
# Cookbook:: greenbone
# Recipe:: source
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

apt_update

node['greenbone']['install']['dependencies'].each do |p|
  package p do
    action :install
  end
end

directory node['greenbone']['install']['dir'] do
  owner 'root'
  group 'root'
  mode   0755
  action :create
end

%w(libraries scanner manager assistant).each do |c|
  ark c do
    url node['greenbone'][c]['url']
    version node['greenbone'][c]['version']
    prefix_root ::Chef::Config[:file_cache_path]
    home_dir ::File.join(::Chef::Config[:file_cache_path], c)
    checksum  node['greenbone'][c]['checksum']
    append_env_path true
    owner 'root'
  end
end

%w(libraries scanner manager assistant).each do |c|
  bash "install_#{c}" do
    action :run
    cwd ::File.join(::Chef::Config[:file_cache_path], c)
    code <<-EOH
    mkdir build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX=#{node['greenbone']['install']['dir']} ..
    make
    make doc
    make install
    EOH
    # environment({'PKG_CONFIG_PATH' => "#{node['greenbone']['install']['dir']}/lib/pkgconfig:$PKG_CONFIG_PATH"})
    #not_if { installed?(s[:installed_files], node['openvas_custom']['install']['dir']) }
  end
end
