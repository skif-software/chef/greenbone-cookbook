name 'greenbone'
maintainer 'Serge A. Salamanka'
maintainer_email 'salamanka.serge@gmail.com'
license 'Apache-2.0'
description 'Installs/Configures Greenbone/OpenVAS'
long_description 'Installs/Configures Greenbone/OpenVAS'
version '0.2.3'
chef_version '>= 12.1' if respond_to?(:chef_version)

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/greenbone/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/greenbone'

depends 'ark'
depends 'runit'
