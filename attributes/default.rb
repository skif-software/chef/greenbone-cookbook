default['greenbone']['install']['dir'] = '/usr'

default['greenbone']['install']['dependencies'] = [
  'build-essential',
  'libfreeradius-client-dev',
  'cmake',
  'pkg-config',
  'libglib2.0-dev',
  'libgpgme11-dev',
  'libgnutls28-dev',
  'uuid-dev',
  'libssh-gcrypt-dev',
  'libhiredis-dev',
  'gcc',
  'pkg-config',
  'libxslt1-dev',
  'libssh-gcrypt-dev',
  'libgnutls28-dev',
  'libglib2.0-dev',
  'libpcap-dev',
  'libgpgme11-dev',
  'flex',
  'bison',
  'libksba-dev',
  'libsnmp-dev',
  'libgcrypt20-dev',
  'libldap2-dev',
  'libssh-dev',
  'gnutls-bin',
  'libsqlite3-dev',
  'libical-dev',
  'libmicrohttpd-dev',
  'libxml2-dev',
  'doxygen',
  'xsltproc',
  'xmltoman',
  'libsql-translator-perl'
]

# Greenbone/OpenVAS-9 components

# Libraries
default['greenbone']['libraries']['version'] = '9.0.2'
default['greenbone']['libraries']['url'] = "https://github.com/greenbone/gvm-libs/releases/download/v#{node['greenbone']['libraries']['version']}/openvas-libraries-#{node['greenbone']['libraries']['version']}.tar.gz"
default['greenbone']['libraries']['checksum'] = '580ee80e39814fa0a718fb652cb97ff46548409cc998cad706462094fe52bc79'

# Scanner
default['greenbone']['scanner']['version'] = '5.1.2'
default['greenbone']['scanner']['url'] = "https://github.com/greenbone/openvas-scanner/archive/v#{node['greenbone']['scanner']['version']}.tar.gz"
default['greenbone']['scanner']['checksum'] = '75cff2f898ff79991c3c6a8cbdbae528b9f4e6f5757aefef0e57be5abb1ec590'
default['greenbone']['scanner']['service_name'] = 'openvas-scanner'
default['greenbone']['scanner']['service_binary'] = File.join(node['greenbone']['install']['dir'], 'sbin', 'openvassd')
default['greenbone']['scanner']['service_parameters'] = []

# Manager
default['greenbone']['manager']['version'] = '7.0.3'
# TODO: rewrite url after 8.0 stable release
default['greenbone']['manager']['url'] = "https://github.com/greenbone/gvm/releases/download/v#{node['greenbone']['manager']['version']}/openvas-manager-#{node['greenbone']['manager']['version']}.tar.gz"
default['greenbone']['manager']['checksum'] = 'cccdfcf39b93a278e853da5a4988397fea2b733894541851b437229556c1e823'
default['greenbone']['manager']['service_name'] = 'openvas-manager'
default['greenbone']['manager']['service_binary'] = File.join(node['greenbone']['install']['dir'], 'sbin', 'openvasmd')
default['greenbone']['manager']['service_parameters'] = []

# GSA ( Greenboon Security Assistant )
default['greenbone']['assistant']['version'] = '7.0.3'
default['greenbone']['assistant']['url'] = "https://github.com/greenbone/gsa/archive/v#{node['greenbone']['assistant']['version']}.tar.gz"
default['greenbone']['assistant']['checksum'] = '0e60d96e6ecf294691f90c7b60228a3abd74cc14106fc365973bf7ca7410d5dd'
default['greenbone']['assistant']['service_name'] = 'openvas-assistant'
default['greenbone']['assistant']['service_binary'] = File.join(node['greenbone']['install']['dir'], 'sbin', 'gsad')
# HINT: to allow access by hostname add this parameter
# default['greenbone']['assistant']['service_parameters'] = ['--allow-header-host="hostname"']
default['greenbone']['assistant']['service_parameters'] = []

# CLI ( Command Line Interface )
default['greenbone']['tools']['version'] = '1.3.1'
default['greenbone']['tools']['url'] = "https://github.com/greenbone/gvm-tools/archive/#{node['greenbone']['tools']['version']}.tar.gz"
default['greenbone']['tools']['checksum'] = '1401ed286d4c1b4909612d1130dfcdc5c8d8e2a6ea076a096c1e11535d1be369'

# Additional packages (ex. texlive-full to enable PDF reports generation)
default['greenbone']['additional_packages'] = []
